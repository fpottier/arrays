module QArray = struct

  (* Most functions are inherited from the module [Array]. *)

  include Array

  (* Types. *)

  type mut
  type immut
  type ('m, 'a) qarray = 'a array
  type ('m, 'a) t = ('m, 'a) qarray

  (* In the future, we might wish to publish the type equality
     [(mut, 'a) qarray = 'a array].
     This could be done by exploiting an equality GADT.
     However, the type [(_, _) eq] does not yet exist in OCaml's
     standard library, so let us wait. *)

  (* The type equality [(immut, 'a) qarray = 'a array]
     must of course not be published. *)

  (* The empty array. *)

  let empty = [||]

  (* Weak type equality witnesses. *)

  let import a = a
  let export a = a

end

(* The implementation of [IArray] exploits the type equality
   [('m, 'a) qarray = 'a array], which is not part of the public
   interface of [QArray]. For this reason, [QArray] and [IArray]
   must be placed in the same file (or [QArray] must publish unsafe
   operations). *)

module IArray = struct

  (* Most functions are inherited from the module [Array]. *)

  include Array

  (* The type ['a iarray]. *)

  type 'a iarray = 'a array

  (* The type ['a t], another name for the same type. *)

  type 'a t = 'a iarray

  (* The empty array. *)

  let empty = [||]

  (* Conversions between mutable arrays and immutable arrays.
     In either direction, a copy is required; we cannot allow
     a single array to be viewed both as mutable and immutable. *)

  let to_array = Array.copy
  let of_array = Array.copy

  (* Sorting requires a copy, followed with an in-place sorting step.
     It might be possible to propose a faster sorting algorithm, by
     taking advantage of the fact that we are allowed to allocate an
     auxiliary array. *)

  let sort cmp a =
    let a = copy a in
    sort cmp a;
    a

  let stable_sort cmp a =
    let a = copy a in
    stable_sort cmp a;
    a

  let fast_sort cmp a =
    let a = copy a in
    fast_sort cmp a;
    a

  (* It would be nice if we could redefine the array access notation
     [a.(i)], but OCaml does not allow this. *)

end
