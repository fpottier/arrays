# ------------------------------------------------------------------------------

# The name of the library.
THIS     := arrays

# The version number is automatically set to the current date,
# unless DATE is defined on the command line.
DATE     := $(shell /bin/date +%Y%m%d)

# The repository URL (https).
REPO     := https://gitlab.inria.fr/fpottier/$(THIS)

# The archive URL (https).
ARCHIVE  := $(REPO)/-/archive/$(DATE)/archive.tar.gz

# ------------------------------------------------------------------------------

.PHONY: all
all:
	@ dune build -p $(THIS)

# [make test] runs all tests.

# Some tests require the following opam packages:
#   regenerate

.PHONY: test
test:
	@ dune build @runtest

# [make versions] compiles this package under many versions of OCaml,
# whose list is specified below.

# This requires appropriate opam switches to exist. A missing switch
# can be created like this:
#   opam switch create 4.03.0

VERSIONS := \
  4.13.0 \

.PHONY: versions
versions:
	@(echo "(lang dune 2.0)" && \
	  for v in $(VERSIONS) ; do \
	    echo "(context (opam (switch $$v)))" ; \
	  done) > dune-workspace.versions
	@ dune build --workspace dune-workspace.versions -p $(THIS)

.PHONY: install
install: all
	@ dune install -p $(THIS)

.PHONY: clean
clean:
	@ rm -f *~ src/*~ dune-workspace.versions
	@ dune clean

.PHONY: uninstall
uninstall:
	@ ocamlfind remove $(THIS) || true

.PHONY: reinstall
reinstall: uninstall
	@ make install

.PHONY: show
show: reinstall
	@ echo "#require \"arrays\";;\n#show Arrays;;" | ocaml

.PHONY: pin
pin:
	@ opam pin add $(THIS) .

.PHONY: unpin
unpin:
	@ opam pin remove $(THIS)

.PHONY: doc
doc:
	dune build @doc
	@echo You can find the documentation in _build/default/_doc/_html/index.html

.PHONY: export
export: doc
	ssh yquem.inria.fr rm -rf public_html/$(THIS)/doc
	scp -r _build/default/_doc/_html yquem.inria.fr:public_html/$(THIS)/doc

.PHONY: release
release:
# Make sure the current version can be compiled and installed.
	@ make uninstall
	@ make clean
	@ make install
# Check the current package description.
	@ opam lint
# Check if everything has been committed.
	@ if [ -n "$$(git status --porcelain)" ] ; then \
	    echo "Error: there remain uncommitted changes." ; \
	    git status ; \
	    exit 1 ; \
	  else \
	    echo "Now making a release..." ; \
	  fi
# Create a git tag.
	@ git tag -a $(DATE) -m "Release $(DATE)."
# Upload. (This automatically makes a .tar.gz archive available on gitlab.)
	@ git push
	@ git push --tags

.PHONY: publish
publish:
# Publish an opam description.
	@ opam publish -v $(DATE) $(THIS) $(ARCHIVE) .
